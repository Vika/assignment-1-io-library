%define CALL_EXIT 60;
%define CALL_WRITE 1;
%define NEW_LINE 0xA;
%define SPACE 0x20;
%define TABLE_SPACE 0x9;

section .text

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, CALL_EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .counter

    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    push rax
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 1
    mov rax, CALL_WRITE
    syscall
    pop rax
    pop rdi
    ret

; Принимает код символа и выводит его в stdout 
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 1
    mov rax, CALL_WRITE
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA) 
print_newline:
    mov rdi, NEW_LINE
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды. 
print_uint:
    xor r8, r8
    
    mov rax, rdi
    mov rdi, 10
    
    .read:
        mov rdx, 0
        div rdi
        add rdx, '0'
        
        push rdx
        
        inc r8
        
        cmp rax, 0
        je .print
        jmp .read
        
    .print:
        pop rdi
        call print_char
        dec r8
        
        test r8, r8
        jne .print
        
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
     test rdi, rdi
     jge .print
     
     push rdi
     mov rdi, '-'
     call print_char
     
     pop rdi
     neg rdi
     
     .print:
          call print_uint
          ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе  
string_equals:
    ;rdi - 1 строка
    ;rsi - 2 строка
    call string_length
    mov r8, rax 
    push rdi
    mov rdi, rsi
    call string_length
    mov r9, rax 
    pop rdi
    cmp r8, r9 
    jne .no
    
    .loop:
         mov r8b, byte[rdi+rax]
         mov r9b, byte[rsi+rax]
         
         cmp r8b, r9b
         jne .no
         
         cmp r8b, 0
         je .yes
         
         inc rax
         
         jmp .loop
    .no:
         mov rax, 0
         ret
    .yes:
         mov rax, 1
          ret     



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока   
read_char:
    push 0
     xor  rax, rax         
     xor  rdi, rdi      
     mov  rsi, rsp  
     mov  rdx, 1
     syscall
     pop rax
     ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор     

read_word:    
    mov r10, rsi
    .tab_loop:
         xor r9, r9
         xor rax, rax

         push rdi
         call read_char
         pop rdi

         mov byte[rdi+r9], al
         inc r9

         cmp rax, SPACE
         je .tab_loop
         
         cmp rax, TABLE_SPACE
         je .tab_loop
         
         cmp rax, NEW_LINE
         je .tab_loop
         
         cmp rax, 0
         je .yes
         
    .main_loop:
         push rdi
         call read_char
         pop rdi

         mov byte[rdi+r9], al

         cmp r10, r9
         jb .no
         inc r9

         cmp rax, SPACE
         je .yes
         
         cmp rax, TABLE_SPACE
         je .yes
         
         cmp rax, NEW_LINE
         je .yes

         cmp rax, 0
         je .yes

         
         jmp .main_loop
         
    .yes:     
         mov rax, rdi
         dec r9
         mov rdx, r9
         mov byte[r9+rdi], 0
         ret
         
    .no:
         xor rax, rax
         ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r9, r9
    xor rax, rax
    mov r8, 10 ; для деления
    xor r10, r10
    
    .check:
         mov r9b, byte[rdi+r10]
         
         cmp r9b, '0'
         jb .end
         cmp r9b, '9'
         ja .end

         inc r10

         sub r9b, "0"
         mul r8
         add rax, r9
      
         jmp .check
         
     .end:
        mov rdx, r10
     	ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
     mov al, byte[rdi]
     cmp al, "-"
     je .neg 
     call parse_uint
     jmp .end
     .neg:
          inc rdi
          call parse_uint
          inc rdx
          neg rax
          ret
     .end:
         ret




; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r9, r9
    
    push rdi 
    push rsi
    push rdx
    
    call string_length
    
    pop rdx
    pop rsi
    pop rdi
    
    cmp rax, rdx 
    jge .no
    
    
    .read:
    	xor r8, r8
    	mov r8b, byte[rdi+r9]
    	mov byte[rsi+r9], r8b 
    	
    	cmp r8b, 0
    	je .end
    	
    	inc r9
    	jmp .read
    
    .no:
    	xor rax, rax
    	jmp .end
    
    .end:
    	ret


